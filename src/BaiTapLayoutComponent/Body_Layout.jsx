import React, { Component } from "react";
import Banner_Layout from "./Banner_Layout";
import Item_Layout from "./Item_Layout";

export default class Body_Layout extends Component {
  render() {
    return (
      <div>
        <Banner_Layout />
        <Item_Layout />
      </div>
    );
  }
}
