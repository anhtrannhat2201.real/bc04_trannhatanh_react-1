import React, { Component } from "react";
import Body_Layout from "./Body_Layout";
import Footer_Layout from "./Footer_Layout";
import Header_Layout from "./Header_Layout";

export default class BaiTapThucHanhLayout extends Component {
  render() {
    return (
      <div>
        <Header_Layout />
        <Body_Layout />
        <Footer_Layout />
      </div>
    );
  }
}
